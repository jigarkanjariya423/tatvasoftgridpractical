package com.jk.firstpractical

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.jk.firstpractical.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityMainBinding
    val list = arrayListOf<Int>()
    private var position:Int? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        mBinding.button.setOnClickListener {
            if(isValid(mBinding.editText.text.toString())){
                list.clear()
                val number = Math.sqrt(mBinding.editText.text.toString().toDouble())
                mBinding.gridLayout.numColumns = number.toInt()
                for (i in 1..mBinding.editText.text.toString().toInt()){
                    list.add(i)
                }
                val customAdapter = MainAdapter(this, list,false,0,false)
                mBinding.gridLayout.adapter = customAdapter
                Handler().postDelayed({
                    val customAdapter = MainAdapter(this, list,true, 0,false)
                    mBinding.gridLayout.adapter = customAdapter
                },3000)
            } else {
                Toast.makeText(this, "Please enter only square root number", Toast.LENGTH_LONG).show()
            }
        }

        mBinding.gridLayout.setOnItemClickListener { adapterView, view, i, l ->
            if(i == position?.plus(1) || position ==  null) {
                position = i
                val customAdapter = MainAdapter(this, list, true, i, true)
                mBinding.gridLayout.adapter = customAdapter
                if (i == list.size - 1) {
                    AlertDialog.Builder(this)
                        .setMessage("You won the game") // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.ok,
                            DialogInterface.OnClickListener { dialog, which ->
                                // Continue with delete operation
                            }) // A null listener allows the button to dismiss the dialog and take no further action.
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show()
                }
            }
        }
    }

    fun isValid(value: String): Boolean {
        val sq = Math.sqrt(value.toDouble())
        return sq - Math.floor(sq) == 0.0
    }
}