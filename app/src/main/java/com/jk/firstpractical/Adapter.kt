package com.jk.firstpractical

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog


internal class MainAdapter(
    private val context: Context,
    private val numbers: ArrayList<Int>,
    private val isColorChange: Boolean,
    private var index: Int,
    private val isClickable: Boolean
) :
    BaseAdapter() {
    private var layoutInflater: LayoutInflater? = null
    private lateinit var imageView: ImageView
    override fun getCount(): Int {
        return numbers.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View? {
        var convertView = convertView
        if (layoutInflater == null) {
            layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        if (convertView == null) {
            convertView = layoutInflater!!.inflate(R.layout.item_grid, null)
        }
        imageView = convertView!!.findViewById(R.id.image)
        if (position == 0 && isColorChange) {
            if (isClickable) {
                imageView.setBackgroundColor(convertView.context.getColor(R.color.purple_500))
            } else {
                imageView.setBackgroundColor(convertView.context.getColor(R.color.design_default_color_error))
            }
        }
        if (isClickable) {
            if (position <= index) {
                imageView.setBackgroundColor(convertView.context.getColor(R.color.purple_500))
            } else if (position == index + 1) {
                imageView.setBackgroundColor(convertView.context.getColor(R.color.design_default_color_error))
            }
        }
        /* if (position == index && isClickable){
             imageView.setBackgroundColor(convertView.context.getColor(R.color.purple_500))
         }*/
        return convertView
    }
}